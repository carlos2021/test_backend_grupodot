from rest_framework.routers import DefaultRouter

from test_backend.users.api.api import UserViewSet

router = DefaultRouter()

router.register("", UserViewSet, basename="users")

urlpatterns = router.urls
