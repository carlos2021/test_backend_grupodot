import requests
from rest_framework.decorators import action
from rest_framework import viewsets
from django.http import JsonResponse
from test_backend.palindrome_integration.utilites.palindrome import sub_palindrome


class SubscriptionWiewsets(viewsets.ViewSet):    
    def retrieve(self, request, pk=None):
        return JsonResponse({"message": sub_palindrome(pk)}, status=200)
