from itertools import combinations


def is_palindrome(text_strings: str) -> bool:
    return text_strings == text_strings[::-1]


def sub_palindrome(text_strings: str) -> str:
    all_sub_palindrome = [
        text_strings[x:y]
        for x, y in combinations(range(len(text_strings) + 1), r=2)
        if is_palindrome(text_strings[x:y])
    ]
    return max(all_sub_palindrome, key=len) if text_strings != "" else ""
