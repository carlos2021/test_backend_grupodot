from django.apps import AppConfig


class PalindromeIntegrationConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "test_backend.palindrome_integration"
