from rest_framework import routers

import test_backend.palindrome_integration.viewsets as vs

router = routers.SimpleRouter()

router.register("palindromo", vs.SubscriptionWiewsets, basename="subscription")

urlpatterns = router.urls
