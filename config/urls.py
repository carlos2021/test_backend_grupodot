from django.contrib import admin
from django.urls import path, include
from config.config_swagger import urlpatternsSwagger

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from test_backend.users.views import Login,Logout, RegisterApi


urlpatterns = urlpatternsSwagger + [
    path('register/',RegisterApi.as_view(), name = 'register'), 
    path('login/',Login.as_view(), name = 'login'),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),  
    path('logout/', Logout.as_view(), name = 'logout'),
    path("", include("test_backend.palindrome_integration.urls")),
]
